#####################################
# ECE271B : Statistical Learning II
# Final Project
# UCSD
# Winter 2018
# Author : Ibrahim Akbar
#####################################

import argparse
import setproctitle
from ..logger.logger import setup_logger
from ..network.regressor import regressor
from ..network.regressor_train import regressor_train
from ..loader.loader_vot import loader_vot
from ..tracker.tracker import tracker
from ..tracker.tracker_trainer_tl import tracker_trainer_tl
from ..tracker.tracker_manager_tl import tracker_manager_tl
from ..train.example_generator import example_generator
from ..loader.video import video

setproctitle.setproctitle('SHOW_TRACKER_VOT_TL')
logger_test = setup_logger(logfile=None)
logger_train = setup_logger(logfile=None)
logger_tracktrain = setup_logger(logfile=None)
logger_example = setup_logger(logfile=None)
logger_comp = setup_logger(logfile=None)


ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", required = True, help = "Path to test prototxt")
ap.add_argument("-t", "--traintxt", required = True, help = "Path to train prototxt")
ap.add_argument("-s", "--solvetxt", required = True, help = "Path to solver prototxt")
ap.add_argument("-model", "--model", required = True, help = "Path to the model")
ap.add_argument("-reset_layers", "--reset_layers", required = True, help = "Names of layers on which to train & reset")
ap.add_argument("-batch_size", "--batch_size", required = False, help = "Initial Size of Generated Samples")
ap.add_argument("-v", "--input", required = True, help = "Path to the vot directory")
ap.add_argument("-lamda_shift", "--lamda_shift", required = True, help = "lamda shift")
ap.add_argument("-lamda_scale", "--lamda_scale", required = True, help = "lamda scale ")
ap.add_argument("-min_scale", "--min_scale", required = True, help = "min scale")
ap.add_argument("-max_scale", "--max_scale", required = True, help = "max scale")
ap.add_argument("-g", "--gpuID", required = True, help = "gpu to use")
ap.add_argument("-rt", "--robust_thresh", default=0, help="Threshhold of Robustness(default: 0)")
ap.add_argument("-waitKey", "--waitKey", default=10, help="Video Wait Time(ms)(default: 10)")
ap.add_argument("-display", "--display", action='store_false', default=True, help="Set to not display")
ap.add_argument("-filename", "--filename", default="Untitled", help="File name of stored metrics")
args = vars(ap.parse_args())


objLoaderVot = loader_vot(args['input'], logger_test)
videos = objLoaderVot.get_videos()

do_train = True
objExampleGen = example_generator(float(args['lamda_shift']), float(args['lamda_scale']), float(args['min_scale']), float(args['max_scale']), logger_example)
objRegTrain = regressor_train(args['traintxt'], args['model'], int(args['gpuID']), args['solvetxt'], logger_train) 
objTrackTrainer = tracker_trainer_tl(objExampleGen, objRegTrain, 2, 0.01, True, logger_tracktrain)

train_set = [objExampleGen, objRegTrain, objTrackTrainer, args['reset_layers']]

# test_net = objRegTrain.regressor.solver.test_nets[0]
# test_net.share_with(objRegTrain.regressor.solver.net)

do_train = False
objRegressor_test = regressor(args['prototxt'], args['model'], args['gpuID'], 1, do_train, logger_test)
objRegressor_comp = regressor(args['prototxt'], args['model'], args['gpuID'], 1, do_train, logger_comp)
objTracker_test = tracker(False, logger_test) # Currently no idea why this class is needed, eventually we shall figure it out
objTracker_comp = tracker(False, logger_comp)

objRegressor_test.net.share_with(objRegTrain.regressor.net)

test_set = [objRegressor_test, objTracker_test, objRegressor_comp, objTracker_comp]

objTrackerVis = tracker_manager_tl(videos, test_set, train_set, args)
record = True
show_ref = True
objTrackerVis.trackAll(0, 1, record, show_ref)
#timer.print_avg_fps()
