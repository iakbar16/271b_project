#####################################
# ECE271B : Statistical Learning II
# Final Project
# UCSD
# Winter 2018
# Author : Ibrahim Akbar
#####################################

from ..train.example_generator import example_generator
from ..helper import config

class tracker_trainer_tl:
    """tracker trainer class"""

    def __init__(self, example_generator, regressor_train, batchsize, decay_r, fixed, logger):
        """TODO: to be defined. """

        self.example_generator_ = example_generator
        self.regressor_train_ = regressor_train
        self.kGeneratedExamplesPerImage = batchsize
        self.kBatchSize = batchsize + 1
        self.fixed = fixed
        self.decay_r = decay_r
        self.logger = logger
        self.images = []
        self.targets = []
        self.bbox_gt_scaled = []


    def process_batch(self):
        """TODO: Docstring for process_batch.
        :returns: TODO

        """
        self.regressor_train_.train(self.images, self.targets,
                self.bbox_gt_scaled)

    def make_training_examples(self):
        """TODO: Docstring for make_training_examples.
        :returns: TODO

        """
        example_generator = self.example_generator_
        image, target, bbox_gt_scaled = example_generator.make_true_example()

        self.images.append(image)
        self.targets.append(target)
        self.bbox_gt_scaled.append(bbox_gt_scaled)

        # Generate more number of examples
        self.images, self.targets, self.bbox_gt_scaled = example_generator.make_training_examples(self.kGeneratedExamplesPerImage, self.images, self.targets, self.bbox_gt_scaled)

        if not self.fixed:
            self.kGeneratedExamplesPerImage = floor(self.kGeneratedExamplesPerImage * (1 - decay_r) ** iteration)
            self.kGeneratedExamplesPerImage = 1 if self.kGeneratedExamplesPerImage == 0 else self.kGeneratedExamplesPerImage


    def train(self, img_prev, img_curr, bbox_prev, bbox_curr):
        """TODO: to be defined. """

        logger = self.logger
        example_generator = self.example_generator_
        example_generator.reset(bbox_prev, bbox_curr, img_prev, img_curr)
        self.make_training_examples()

        num_train_samples = len(self.images)

        if num_train_samples < 0:
            logger.error('Error: num_use = {}', num_train_samples)
            return

        if (num_train_samples != self.kBatchSize):
            logger.error('Error: image batch size != batch size')
            return

        self.process_batch()

        self.images, self.targets, self.bbox_gt_scaled = [], [], []


    def get_loss(self, image_prev, img_curr, bbox_prev, bbox_curr_gt):

        logger = self.logger
        example_generator = self.example_generator_
        example_generator.reset(bbox_prev, bbox_curr_gt, image_prev, img_curr)
        image, target, bbox_gt_scaled = example_generator.make_true_example()

        self.images.append(image)
        self.targets.append(target)
        self.bbox_gt_scaled.append(bbox_gt_scaled)

        num_train_samples = len(self.images)

        if num_train_samples < 0:
            logger.error('Error: num_use = {}', num_train_samples)
            return


        loss = self.regressor_train_.get_loss(self.images, self.targets, self.bbox_gt_scaled)

        self.images, self.targets, self.bbox_gt_scaled = [], [], []

        return loss

    def re_init(self, file):

        self.regressor_train 