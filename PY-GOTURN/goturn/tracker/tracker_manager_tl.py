#####################################
# ECE271B : Statistical Learning II
# Final Project
# UCSD
# Winter 2018
# Author : Ibrahim Akbar
#####################################

import pickle
import cv2
import copy as cp
from tqdm import tqdm
import numpy as numpy
import matplotlib.pyplot as plt
from ..utils.metrics import metric_recorder
from ..network.regressor import regressor
from ..network.regressor_train import regressor_train
from ..logger.logger import setup_logger
opencv_version = cv2.__version__.split('.')[0]

import caffe
from caffe.proto import caffe_pb2
from google.protobuf import text_format


class tracker_manager_tl:

    """Docstring for tracker_manager. """

    def __init__(self, videos, test_set, train_set,  args, reinitialization=True):
        """This is

        :videos: list of video frames and annotations
        :test_set: list of test objects
        :train_set: list of train object
        :timer: tracks computation time per frame
        :returns: list of video sub directories
        """

        self.videos = videos
        self.test_set = test_set
        self.train_set = train_set
        self.metrics = {}
        self.waitKey = args['waitKey']
        self.display = args['display']
        self.reinitialization = reinitialization
        self.file_name = args['filename']
        self.args = args

        solver_config = caffe_pb2.SolverParameter()
        with open('./nets/solver.prototxt') as f:
            text_format.Merge(str(f.read()), solver_config)

        new_solver_config = text_format.MessageToString(solver_config)
        with open('./nets/temp.prototxt', 'w') as f:
            f.write(new_solver_config)



    def restore_weights(self, layers, net_to):
        """
        Copy weights between networks.
            
        :param net_from: network to copy weights from
        :type net_from: caffe.Net
        :param net_to: network to copy weights to
        :type net_to: caffe.Net
        """
        bkup_weights = self.bkup_weights
        bkup_biases = self.bkup_biases

        for i, name in enumerate(layers):

            net_to.params[name][1] = bkup_biases[i]
            net_to.params[name][0] = bkup_weights[i]


    def set_backup_weights(self, layers, net):

        self.bkup_weights = []
        self.bkup_biases = []

        for i, name in enumerate(layers):
            self.bkup_weights.append(net.params[name][0]) # Dont actually know which index is which but...meh
            self.bkup_biases.append(net.params[name][1])



    def trackAll(self, start_video_num, pause_val, record, show_ref):
        """Track the objects in the video
        """

        videos = self.videos
        if self.display:
            fig = plt.figure()
        objRegressor_test = self.test_set[0]
        objTracker_test = self.test_set[1]

        objRegressor_comp = self.test_set[2]
        objTracker_comp = self.test_set[3]

        test_net = objRegressor_test.net
        

        objRegressor_train = self.train_set[1]
        objTracker_trainer = self.train_set[2]
        training_layers = self.train_set[3]

        solver = objRegressor_train.regressor.solver
        layer_names = training_layers.split(" ")

        self.set_backup_weights(layer_names, test_net)

        train_net = solver.net 

        metrics = self.metrics

        video_keys = list(videos.keys())
        for i in tqdm(range(start_video_num, len(videos))):

            if i > 0:
                self.restore_weights(layer_names, test_net)
                self.restore_weights(layer_names, train_net)
                logger_t = setup_logger(logfile=None)
                # objRegressor_train = regressor(self.args['traintxt'], self.args['model'], int(self.args['gpuID']), 3, True, logger_t, self.args['solvetxt'])
                objTracker_trainer.regressor_train_.__init__(self.args['traintxt'], self.args['model'], int(self.args['gpuID']), self.args['solvetxt'], logger_t) 
                solver = objTracker_trainer.regressor_train_.regressor.solver
                train_net = solver.net

            video_frames = videos[video_keys[i]][0]
            annot_frames = videos[video_keys[i]][1]
            video_name = video_keys[i]
            num_frames = min(len(video_frames), len(annot_frames))

            # Get the first frame of this video with the intial ground-truth bounding box
            prev_frame = video_frames[0]
            prev_bbox = annot_frames[0]
            prev_sMatImage = cv2.imread(prev_frame)

            objTracker_test.init(prev_sMatImage, prev_bbox, objRegressor_test)

            if show_ref:
                objTracker_comp.init(prev_sMatImage, prev_bbox, objRegressor_comp)
            metrics[video_name] = metric_recorder()
	    cv2.waitKey(0)
            for i in tqdm(range(1, num_frames)):

                if record:
                    start = metrics[video_name].timer.tic()

                curr_frame = video_frames[i]
                curr_sMatImage = cv2.imread(curr_frame)
                curr_sMatImageDraw = curr_sMatImage.copy()
                curr_bbox_gt = annot_frames[i]
                curr_bbox = annot_frames[i]
                
                if opencv_version == '2':
                    cv2.rectangle(curr_sMatImageDraw, (int(curr_bbox.x1), int(curr_bbox.y1)), (int(curr_bbox.x2), int(curr_bbox.y2)), (255, 255, 255), 2)
                else:
                    curr_sMatImageDlraw = cv2.rectangle(curr_sMatImageDraw, (int(curr_bbox.x1), int(curr_bbox.y1)), (int(curr_bbox.x2), int(curr_bbox.y2)), (255, 255, 255), 2)

                if show_ref:
                    curr_bbox_ref = objTracker_comp.track(curr_sMatImage, objRegressor_comp)
                    
                    if opencv_version == '2':
                        cv2.rectangle(curr_sMatImageDraw, (int(curr_bbox_ref_.x1), int(curr_bbox_ref.y1)), (int(curr_bbox_ref.x2), int(curr_bbox_ref.y2)), (0, 0, 255), 2)
                    else:
                        curr_sMatImageDraw = cv2.rectangle(curr_sMatImageDraw, (int(curr_bbox_ref.x1), int(curr_bbox_ref.y1)), (int(curr_bbox_ref.x2), int(curr_bbox_ref.y2)), (0, 0, 255), 2)


                curr_bbox = objTracker_test.track(curr_sMatImage, objRegressor_test)

                if opencv_version == '2':
                    cv2.rectangle(curr_sMatImageDraw, (int(curr_bbox.x1), int(curr_bbox.y1)), (int(curr_bbox.x2), int(curr_bbox.y2)), (255, 0, 0), 2)
                else:
                    curr_sMatImageDraw = cv2.rectangle(curr_sMatImageDraw, (int(curr_bbox.x1), int(curr_bbox.y1)), (int(curr_bbox.x2), int(curr_bbox.y2)), (255, 0, 0), 2)
                
                if self.display:
                    cv2.imshow(video_name, curr_sMatImageDraw)
                    cv2.waitKey(self.waitKey)
                    plt.plot(metrics[video_name].IoU)
                    fig.canvas.draw()

                if record:
                    loss = objTracker_trainer.get_loss(prev_sMatImage, curr_sMatImage, prev_bbox, curr_bbox_gt)
                    metrics[video_name].update_loss(loss)
                    
                objTracker_trainer.train(prev_sMatImage, curr_sMatImage, prev_bbox, curr_bbox)

                test_net.share_with(train_net)

                if record:

                    end = metrics[video_name].timer.toc(start)
                    metrics[video_name].update_IoU(curr_bbox_gt, curr_bbox)

                    if show_ref:
                        metrics[video_name].update_IoU_ref(curr_bbox_gt, curr_bbox_ref)

                    metrics[video_name].timer.store_time(end)
                    metrics[video_name].timer.update_avg()
                    metrics[video_name].print_metrics()
                
                prev_sMatImage = curr_sMatImage
                prev_bbox = curr_bbox

                if self.reinitialization and metrics[video_name].get_latest_IoU() <= 0:
                    objTracker_test.init(prev_sMatImage, curr_bbox_gt, objRegressor_test)
                if show_ref and metrics[video_name].IoU_ref[-1] <= 0:
                    objTracker_comp.init(prev_sMatImage, curr_bbox_gt, objRegressor_comp)
<<<<<<< HEAD
            # cv2.destroyWindow(video_name)
=======
            if self.display:
                cv2.destroyWindow(video_name)
>>>>>>> c8039c5a26b7c00e291893ff5a75a72fefd38407
        f = open("./data/"+self.file_name,'wb')
        pickle.dump(metrics, f)
