#####################################
# ECE271B : Statistical Learning II
# Final Project
# UCSD
# Winter 2018
# Author : Ibrahim Akbar
#####################################

import timeit


class timer:

    def __init__(self):

        self.times = []
        self.avg = None

    def tic(self):

        return timeit.default_timer()

    def toc(self, start):

        return timeit.default_timer()-start

    def store_time(self, value):

        self.times.append(value)

    def update_avg(self):

        n = float(len(self.times))

        if n == 1:

            self.avg = self.times[-1]

        else:

            self.avg = (1 / n) * (self.times[-1] + (n - 1) * self.avg)

    def print_avg_fps(self):

        print("Average Time: {0}".format(str(1 / self.avg)))