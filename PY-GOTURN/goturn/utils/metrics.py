#####################################
# ECE271B : Statistical Learning II
# Final Project
# UCSD
# Winter 2018
# Author : Ibrahim Akbar
#####################################


from tqdm import tqdm
from ..utils.timer import timer

class metric_recorder:


    def __init__(self):

        # Index 0 is the average
        self.timer = timer()
        self.IoU = []
        self.IoU_ref = []
        self.loss = []
        self.rob_thresh = 0
        self.avg_loss = 0
        self.avg_loss_ref = 0
        self.avg_IoU = 0
        self.avg_IoU_ref = 0
        self.robustness = 0
        self.real_robostness = 0
        self.robustness_ref = 0
        self.faults = []
        self.faults_ref = []

    def update_loss(self, loss):

        if not loss:
            pass
        self.loss.append(loss)
        n = len(self.loss)
        self.avg_loss = sum(self.loss)*1.0/n


    def update_IoU(self, bbox_gt, bbox_est):
        
        iou = self.pixel_IoU(bbox_gt, bbox_est)

        self.IoU.append(iou)
        n = len(self.IoU)
        self.avg_IoU = sum(self.IoU)*1.0/n
        self.robustness = sum([i>self.rob_thresh for i in self.IoU])*1.0/n
        self.real_robostness = sum([i==self.rob_thresh for i in self.IoU])*1.0/n
        self.faults += [iou>self.rob_thresh]

    def update_IoU_ref(self, bbox_gt, bbox_est):
        
        iou = self.pixel_IoU(bbox_gt, bbox_est)

        self.IoU_ref.append(iou)
        n = len(self.IoU_ref)
        self.avg_IoU_ref = sum(self.IoU_ref)*1.0/n
        self.robustness_ref = sum([i>self.rob_thresh for i in self.IoU_ref])*1.0/n
        self.faults_ref += [iou>self.rob_thresh]

    def pixel_IoU(self, bbox_gt, bbox_est):

        gt_x_min = min([bbox_gt.x1, bbox_gt.x2])
        gt_x_max = max([bbox_gt.x1, bbox_gt.x2])

        gt_y_min = min([bbox_gt.y1, bbox_gt.y2])
        gt_y_max = max([bbox_gt.y1, bbox_gt.y2])

        est_x_min = min([bbox_est.x1, bbox_est.x2])
        est_x_max = max([bbox_est.x1, bbox_est.x2])

        est_y_min = min([bbox_est.y1, bbox_est.y2])
        est_y_max = max([bbox_est.y1, bbox_est.y2])

        y_max = max(gt_y_min, est_y_min)
        y_min = min(gt_y_max, est_y_max)

        x_max = max(gt_x_min, est_x_min)
        x_min = min(gt_x_max, est_x_max)

        dx = (x_min - x_max)
        dy = (y_min - y_max)
        
        if (dx >= 0) and (dy >= 0):
            inter = dx * dy
        else:
            inter = 0

        gt_area = (gt_x_max - gt_x_min) * (gt_y_max - gt_y_min)
        est_area = (est_x_max - est_x_min) * (est_y_max - est_y_min)

        iou = inter*1.0 / (gt_area + est_area - inter)

        return iou

    def get_latest_IoU(self):
        return self.IoU[-1]
    def print_metrics(self):
        tqdm.write("Average Loss: {0}".format(self.avg_loss))
        tqdm.write("Average IoU: {0}".format(self.avg_IoU))
        tqdm.write("Latest  IoU: {}".format(self.IoU[-1]))
        tqdm.write("Robustness: {}".format(self.robustness))
        #self.timer.print_avg_fps()
