DEPLOY_PROTO='./nets/tracker.prototxt'		 
DEPLOY_PROTO_TRAIN='./nets/tracker_train.prototxt'
SOLVE_PROTO='./nets/solver.prototxt'
CAFFE_MODEL='./nets/tracker.caffemodel'		
TEST_DATA_PATH='../../vot2014'

RESET_LAYERS='fc8-shapes'		
for i in {1..4}
do
SOLVE_PROTO=./nets/exp_lr1e6_gammap1e${i}_p2.prototxt
python -m goturn.test.show_tracker_vot_tl \
	--p $DEPLOY_PROTO \
	--t $DEPLOY_PROTO_TRAIN \
	--s $SOLVE_PROTO \
	--model $CAFFE_MODEL \
	--reset_layers $RESET_LAYERS \
	--i $TEST_DATA_PATH \
	--lamda_shift 5 \
	--lamda_scale 15 \
	--min_scale -0.4 \
	--max_scale 0.4 \
	--g 0 \
	--display \
	--filename exp2_lr1e6_gammap1e${i}_p2.pickle
done
